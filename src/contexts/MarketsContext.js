import React from 'react';

const MarketsContext = React.createContext({
    selectedSelections: [],
    updateExpandedMarkets: () => {},
    setBtnSelected: () => {},
});

export default MarketsContext;
