import React, {useEffect, useRef, useState} from 'react';
import style from './navigation.scss'
import $ from "jquery";
import "jquery.kinetic";

export default function Navigation({store, changeNavTab, currentNavigationTabId}) {
  const [activeNavigationTab, setActiveNavigationTab] = useState(currentNavigationTabId);
  const navRef = useRef(null);
  useEffect(() => {
    $(".navRef").kinetic({
      filterTarget({tagName}, {type}) {
        if (!/down|start/.test(type)) return !/area|a|input|div|img/i.test(tagName);
      }
    });
  }, []);
  return (
    <div className={`${style.navigation} navRef`} ref={navRef}>
      {store.sort((a, b) => a.displayOrder - b.displayOrder).map(({id, imageUrl, floatingImageUrl, description}, i) => {
        return (
          <div
            key={id}
            onClick={() => {
              if (navRef.current !== null) $(navRef.current).animate({scrollLeft: (100 / 1.5) * i}, 200);
              setActiveNavigationTab(id);
              changeNavTab(id)
            }}
            className={`${style.item} ${id === activeNavigationTab ? style.active : ''}`}
          >
            <div className={style.icon}>
              <div className={style.default}>
                <img src={imageUrl} alt=""/>
              </div>
              <div className={style.hover}>
                <img src={floatingImageUrl} alt=""/>
              </div>
            </div>
            <div className={style.title}>{description}</div>
          </div>
        )
      })}
    </div>
  )
}
