import React from 'react';
import style from './App.scss';

export default React.memo(({ leaguesData, title, goBack }) => (
  <div className={style.backButton} onClick={() => goBack(leaguesData)}>
    <span><i className="fas fa-arrow-left"/></span>
    <span>{title}</span>
  </div>
), (prevProps, nextProps) => prevProps.title === nextProps.title);
