import React, {useEffect, useState} from 'react';
import _ from 'lodash';
import Navigation from '../navigation/navigation';
import Wrap from '../wrap/wrap';
import {getFullAppData, getPricesForSelections, getTeamNames, placeBet,} from '../../api/const';
import style from './App.scss';
import MarketsContext from '../../contexts/MarketsContext';
import AppHeader from './AppHeader';
import Footer from "../footer/footer";
import Preloader from "../preloader/preloader";
import {getClosestSelectionForOptimalLine} from "../utils";

const initialState = {
  currentNavigationTabId: 1,
  countingDenumerator: null,
  selectedSelections: [],
  oldStore: [],
  store: [],
  teamNames: [],
  countStake: 0,
  isDataUpdating: false,
};

function getSelectionsWithDifferentTypes({selections, yesNoSelections, playerStatsSelections}) {
  if (selections && selections.length > 0) return {selectionsValue: selections, selectionsType: 'selections'};
  if (yesNoSelections && yesNoSelections.length > 0) return {selectionsValue: yesNoSelections, selectionsType: 'yesNoSelections'};
  if (playerStatsSelections && playerStatsSelections.length > 0)
    return {selectionsValue: playerStatsSelections, selectionsType: 'playerStatsSelections'};
  return { selectionsValue: [], selectionsType: 'unknown' }
}

function getSelectionsFromYesNo(selections) {
  const tabSelections = [];
  selections.forEach(({groupedName, noSelection, yesSelection}) =>
    tabSelections.push({...noSelection, tabName: groupedName}, {...yesSelection, tabName: groupedName}));
  return tabSelections;
}

function addSelectionsToYesNo(selections, updatedSelections) {
  return selections.map(selection => {
    const {yesSelection, noSelection} = selection;
    const updatedNoSelection = updatedSelections.filter(({definitionId}) => definitionId === noSelection.definitionId);
    const updatedYesSelection = updatedSelections.filter(({definitionId}) => definitionId === yesSelection.definitionId);
    return {
      ...selection,
      noSelection: updatedNoSelection.length > 0 ? updatedNoSelection[0] : noSelection,
      yesSelection: updatedYesSelection.length > 0 ? updatedYesSelection[0] : yesSelection,
    }
  });
}

function getSelectionsFromPlayerStats(selects) {
  const tabSelections = [];
  selects.forEach(({selections}) => selections.forEach(selection => tabSelections.push(selection)));
  return tabSelections;
}

function addSelectionsToPlayerStats(selects, updatedSelections) {
  return selects.map(select => ({
    ...select, selections: select.selections.map((selection) => {
      const updatedSelection = updatedSelections.filter(({definitionId}) => definitionId === selection.definitionId);
      if (updatedSelection.length > 0) return updatedSelection[0];
      return selection;
    })
  }));
}

function collectSelectionsFromDifferentStructures(selections, selectionsType) {
  if (selectionsType === 'yesNoSelections') return getSelectionsFromYesNo(selections);
  if (selectionsType === 'playerStatsSelections') return getSelectionsFromPlayerStats(selections);
  return selections;
}

function restorePreviousSelectionsStructure(selections, selectionsType, updatedSelections) {
  if (selectionsType === 'yesNoSelections')
    return addSelectionsToYesNo(selections, updatedSelections);
  if (selectionsType === 'playerStatsSelections')
    return addSelectionsToPlayerStats(selections, updatedSelections);
  return selections.map((selection) => {
    const updatedSelection = updatedSelections.filter(({definitionId, betSlipName}) => definitionId === selection.definitionId
    && betSlipName === selection.betSlipName);
    if (updatedSelection.length > 0) return updatedSelection[0];
    return selection;
  });
}

const hasDuplicate = (array, value) => array.some(({definitionId}) => definitionId === value.definitionId);

function collectExpandedSelections(updatedStore, navigationTabId, marketName) {
  const allSelections = [];
  const selectedSelections = [];
  const visibleSelections = [];

  updatedStore.forEach(({id, markets}) => {
    markets.forEach(({tabs}) => {
      tabs.forEach((tab) => {
        const {selectionsValue, selectionsType} = getSelectionsWithDifferentTypes(tab.aggregateSelections);
        collectSelectionsFromDifferentStructures(selectionsValue, selectionsType)
          .forEach((selection) => {
            if (selection.isSelected && !hasDuplicate(selectedSelections, selection)) {
              selectedSelections.push({...selection, tabName: tab.subNavLinkName, marketName: tab.displayName ,tabNameForYesNo : selection.tabName});
            }
          });
      });
    });

    function collectSelections(tabs) {
      tabs.forEach((tab) => {
        if (tab.isExpanded) {
          const {selectionsValue, selectionsType} = getSelectionsWithDifferentTypes(tab.aggregateSelections);
          const selections = collectSelectionsFromDifferentStructures(selectionsValue, selectionsType);
          if (tab.template === 10) {
            const vSelections = selections.filter(({visibleSelection}) => visibleSelection);
            const selection = selections.filter(({homeScore = 0, awayScore = 0}) =>
              Number(awayScore) === 0 && Number(homeScore) === 0)[0];
            if (vSelections.length > 0) {
              vSelections.forEach(vSelection => {
                if (!vSelection.isSelected && !hasDuplicate(visibleSelections, vSelection)) {
                  visibleSelections.push({...vSelection, visibleSelection: true});
                }
                if (!hasDuplicate(allSelections, vSelection)) allSelections.push({...vSelection, visibleSelection: true});
              });
            } else {
              if (!selection.isSelected && !hasDuplicate(visibleSelections, selection)) {
                visibleSelections.push({...selection, visibleSelection: true});
              }
              if (!hasDuplicate(allSelections, selection)) allSelections.push({...selection, visibleSelection: true});
            }
          } else if (tab.template === 5) {
            const vSelections = selections.filter(({visibleSelection}) => visibleSelection);
            if (vSelections.length > 0) {
              vSelections.forEach(vSelection => {
                if (!vSelection.isSelected && !hasDuplicate(visibleSelections, vSelection)) {
                  visibleSelections.push({...vSelection, visibleSelection: true});
                }
                if (!hasDuplicate(allSelections, vSelection)) allSelections.push({...vSelection, visibleSelection: true});
              });
            } else {
              selectionsValue.forEach(({optimalLine, selections}) => {
                const defaultSelection = selections.filter(selection => selection.totalsLine === optimalLine)[0];
                if (defaultSelection) {
                  if (!defaultSelection.isSelected && !hasDuplicate(visibleSelections, defaultSelection)) {
                    visibleSelections.push({...defaultSelection, visibleSelection: true});
                  }
                  if (!hasDuplicate(allSelections, defaultSelection)) allSelections.push({...defaultSelection, visibleSelection: true});
                } else {
                  const closestSelection = getClosestSelectionForOptimalLine(optimalLine, selections);
                  if (!closestSelection.isSelected && !hasDuplicate(visibleSelections, closestSelection)) {
                    visibleSelections.push({...closestSelection, visibleSelection: true});
                  }
                  if (!hasDuplicate(allSelections, closestSelection)) allSelections.push({...closestSelection, visibleSelection: true});
                }
              });
            }
          } else {
            selections.forEach(selection => {
              if (!selection.isSelected && !hasDuplicate(visibleSelections, selection)) {
                visibleSelections.push(selection);
              }
              if (!hasDuplicate(allSelections, selection)) allSelections.push(selection);
            });
          }
        }
      });
    }

    if (id === navigationTabId) {
      if (marketName) {
        markets.forEach(({displayName, isExpanded, tabs}) => {
          if (isExpanded && displayName === marketName) collectSelections(tabs);
        });
      } else {
        markets.forEach(({displayName, isExpanded, tabs}) => {
          if (isExpanded) collectSelections(tabs);
        });
      }
    }
  });

  return {
    allSelections: {
      values: allSelections,
      ids: allSelections.map(({definitionId}) => definitionId),
    },
    selectedSelections: {
      values: selectedSelections,
      ids: selectedSelections.map(({definitionId}) => definitionId),
    },
    visibleSelections: {
      values: visibleSelections,
      ids: visibleSelections.map(({definitionId}) => definitionId),
    },
  }
}

export default function App({competition, changeId, leaguesData, selectedSport: {isAwayTeamDisplayedFirst}}) {
  const {title, fixtureId} = competition;
  const [appData, setAppData] = useState(initialState);
  const {
    currentNavigationTabId, countingDenumerator, store, teamNames, countStake, oldStore, isDataUpdating,
  } = appData;

  useEffect(() => downloadAppData(), []);

  function updateDifferentSelections(selectedSelection, tab, selections, updatedSelections) {
    if (selectedSelection && tab.template === 10) {
      return selections.map(selection => {
        if (selection.definitionId === updatedSelections[0].definitionId) {
          return {...selection, visibleSelection: true};
        }
        return {...selection, visibleSelection: false};
      });
    }

    if (selectedSelection && tab.template === 5) {
      return selections.map(select => {
        if (select.selections.some(selection => selection.definitionId === updatedSelections[0].definitionId)) {
          return {
            ...select,
            selections: select.selections.map(selection => {
              if (selection.definitionId === updatedSelections[0].definitionId) {
                return {...selection, visibleSelection: true};
              }
              return {...selection, visibleSelection: false};
            })
          }
        }

        return select;
      });
    }

    return selections;
  }

  function updateTabsSelections(
    updatedStore, updatedSelections, selectedNavigationTabId = false, betSlipPrice = false, receivedTeamNames = false,
    selectedSelection,
  ) {
    const newStore = updatedStore.map(navigationTab => {
      if (navigationTab.id === (selectedNavigationTabId ? selectedNavigationTabId : currentNavigationTabId)) {
        return {
          ...navigationTab,
          markets: navigationTab.markets.map((market) =>
            market.isExpanded ? ({
              ...market,
              tabs: market.tabs.map((tab) => {
                if (tab.isExpanded) {
                  const {selectionsValue, selectionsType} = getSelectionsWithDifferentTypes(tab.aggregateSelections);
                  const selections = restorePreviousSelectionsStructure(
                    selectionsValue,
                    selectionsType,
                    updatedSelections,
                  );
                  return {
                    ...tab,
                    aggregateSelections: {
                      ...tab.aggregateSelections,
                      [selectionsType]: updateDifferentSelections(
                        selectedSelection,
                        tab,
                        selections,
                        updatedSelections,
                      ),
                    },
                  };
                }
                return tab;
              }),
            }) : market),
        }
      }
      return navigationTab;
    });

    setAppData({
      ...appData,
      currentNavigationTabId: selectedNavigationTabId ? selectedNavigationTabId : currentNavigationTabId,
      countingDenumerator: betSlipPrice ? betSlipPrice : countingDenumerator,
      teamNames: receivedTeamNames ? receivedTeamNames : teamNames,
      oldStore: receivedTeamNames ? newStore : oldStore,
      store: newStore,
      isDataUpdating: false,
    });
  }

  function updateSelectionsPrices(selections, prices) {
    return selections.map((selection) => {
      const selectionPrice = prices.filter(({selectionId}) => selectionId === selection.definitionId);
      if (selectionPrice.length > 0) return {...selection, ...selectionPrice[0].price};
      return selection;
    });
  }

  function updateExpandedMarkets(
    updatedStore = store,
    selectedNavigationTabId = false,
    receivedTeamNames = false,
    marketName = false,
    selectedSelection = false,
  ) {
    const {
      allSelections, visibleSelections, selectedSelections,
    } = collectExpandedSelections(
      updatedStore,
      selectedNavigationTabId ? selectedNavigationTabId : currentNavigationTabId,
      marketName,
    );

    if (selectedSelection || visibleSelections.ids.length > 0 || selectedSelections.ids.length > 0 || marketName) {
      getPricesForSelections(
        selectedSelection && !selectedSelection.isSelected ? [selectedSelection.definitionId] : visibleSelections.ids,
        fixtureId,
        selectedSelections.ids,
      ).then(({selectionPrices, betSlipPrice}) => {
        updateTabsSelections(
          updatedStore,
          updateSelectionsPrices(
            selectedSelection ? [selectedSelection] : allSelections.values.map(selection => {
              if (selection.isSelected) return {...selection, ...betSlipPrice};
              return selection;
            }),
            selectionPrices,
          ),
          selectedNavigationTabId,
          betSlipPrice,
          receivedTeamNames,
          selectedSelection,
        )
      });
    } else {
      updateTabsSelections(
        updatedStore,
        updateSelectionsPrices(
          allSelections.values,
          [],
        ),
        selectedNavigationTabId,
        undefined,
        receivedTeamNames,
        selectedSelection,
      )
    }
  }

  function downloadAppData() {
    getFullAppData(fixtureId).then((data) => {
      const store = data.map((navigationTab) => {
        const groupedTabData =
          _(navigationTab.markets)
            .groupBy(({displayName}) => displayName)
            .map(tabs => ({displayName: tabs[0].displayName, tabs, displayOrder: tabs[0].displayOrder}))
            .value();

        let newMarkets = [];

        if (groupedTabData.length > 1 && groupedTabData[groupedTabData.length - 1].tabs.some(tab => tab.id === 500)) {
          newMarkets = groupedTabData.pop().tabs.map(market => ({
            displayName: market.displayName,
            tabs: [market],
          }));
        }

        const markets = [...groupedTabData, ...newMarkets].map((market) => {
          if (market.tabs.some(({isExpanded}) => isExpanded)) return {...market, isExpanded: true};
          return market;
        });

        return {...navigationTab, markets};
      });
      getTeamNames(fixtureId).then(teamNames => {
        updateExpandedMarkets(store, false, teamNames)
      });
    });
  }

  function updateTabSelectionPrices(updatedStore, updatedSelections, tabId) {
    setAppData({
      ...appData, store: updatedStore.map(navigationTab => {
        if (navigationTab.id === currentNavigationTabId) {
          return {
            ...navigationTab,
            markets: navigationTab.markets.map((market) =>
              market.isExpanded ? ({
                ...market,
                tabs: market.tabs.map((tab) => {
                  if (tabId === tab.id) {
                    const {selectionsValue, selectionsType} = getSelectionsWithDifferentTypes(tab.aggregateSelections);
                    return {
                      ...tab,
                      aggregateSelections: {
                        ...tab.aggregateSelections,
                        [selectionsType]: restorePreviousSelectionsStructure(
                          selectionsValue,
                          selectionsType,
                          updatedSelections,
                        ),
                      },
                    };
                  }
                  return tab;
                }),
              }) : market),
          }
        }
        return navigationTab;
      })
    })
  }

  function getTabSelectionData(selectedTab, marketName, correctScore) {
    const {selectedSelections} = collectExpandedSelections(store, currentNavigationTabId);
    let visibleSelections = [];
    const updatedStore = store.map(navigationTab => ({
      ...navigationTab,
      markets: navigationTab.markets.map((market) => {
        if (market.displayName === marketName) {
          return {
            ...market,
            tabs: market.tabs.map((tab) => {
              if (selectedTab.id === tab.id) {
                const {selectionsValue, selectionsType} = getSelectionsWithDifferentTypes(tab.aggregateSelections);
                const collectedSelections = collectSelectionsFromDifferentStructures(selectionsValue, selectionsType);
                if (tab.template === 10) {
                  const selection = collectedSelections.filter(({homeScore = 0, awayScore = 0}) =>
                    Number(awayScore) === correctScore.awayScore
                    && Number(homeScore) === correctScore.homeScore)[0];
                  if (!selection.isSelected && !hasDuplicate(visibleSelections, selection)) {
                    visibleSelections.push({...selection, visibleSelection: true});
                  }
                } else if (tab.template === 5) {
                  const vSelections = collectedSelections.filter(({visibleSelection}) => visibleSelection);
                  if (vSelections.length > 0) {
                    vSelections.forEach(vSelection => {
                      if (!vSelection.isSelected && !hasDuplicate(visibleSelections, vSelection)) {
                        visibleSelections.push({...vSelection, visibleSelection: true});
                      }
                    });
                  } else {
                    selectionsValue.forEach(({optimalLine, selections}) => {
                      const defaultSelection = selections.filter(selection => selection.totalsLine === optimalLine)[0];
                      if (defaultSelection) {
                        if (!defaultSelection.isSelected && !hasDuplicate(visibleSelections, defaultSelection)) {
                          visibleSelections.push({...defaultSelection, visibleSelection: true});
                        }
                      } else {
                        const closestSelection = getClosestSelectionForOptimalLine(optimalLine, selections);
                        if (!closestSelection.isSelected && !hasDuplicate(visibleSelections, closestSelection)) {
                          visibleSelections.push({...closestSelection, visibleSelection: true});
                        }
                      }
                    });
                  }
                } else visibleSelections = collectedSelections;
                return {...tab, isExpanded: true};
              }
              return {...tab, isExpanded: false};
            }),
          };
        }
        return market;
      }),
    }));

    getPricesForSelections(
      visibleSelections.map(({definitionId}) => definitionId),
      fixtureId,
      selectedSelections.ids,
    ).then(({selectionPrices, betSlipPrice}) =>
      updateTabSelectionPrices(
        updatedStore,
        updateSelectionsPrices(
          visibleSelections.map(selection => {
            if (selection.isSelected) return {...selection, ...betSlipPrice};
            return selection;
          }),
          selectionPrices,
        ),
        selectedTab.id,
      ))
  }

  function handleSingleSelection(tab, selections, selectedBtn, selectionsType, selectionsValue) {
    return {
      ...tab,
      aggregateSelections: {
        ...tab.aggregateSelections,
        [selectionsType]: restorePreviousSelectionsStructure(
          selectionsValue,
          selectionsType,
          selections.map((selection) => {
            if (selection.definitionId === selectedBtn.definitionId) {
              if (selection.isSelected) return {...selection, isSelected: false};
              return {...selection, isSelected: true};
            }
            if ('responseCode' in selection && selection.responseCode === 2) return {...selection, isSelected: false};
            return selection;
          })),
      },
    };
  }

  function handleMultiSelection(tab, selections, selectedBtn, selectionsType, selectionsValue) {
    return {
      ...tab,
      aggregateSelections: {
        ...tab.aggregateSelections,
        [selectionsType]: restorePreviousSelectionsStructure(
          selectionsValue,
          selectionsType,
          selections.map((selection) => {
            if (selectedBtn.definitionId === selection.definitionId) {
              if (selection.isSelected) return {...selection, isSelected: false};
              if (!tab.singleSelectionOnly) return {...selection, isSelected: true};
            }
            if ('responseCode' in selection && selection.responseCode === 2) return {...selection, isSelected: false};
            return selection;
          })),
      },
    };
  }

  function setBtnSelected(selectedBtn) {
    const updatedStore = store.map((navigationTab) => {
      return {
        ...navigationTab,
        markets: navigationTab.markets.map((market) => {
          return {
            ...market,
            tabs: market.tabs.map((tab) => {
              const {selectionsValue, selectionsType} = getSelectionsWithDifferentTypes(tab.aggregateSelections);
              const selections = collectSelectionsFromDifferentStructures(selectionsValue, selectionsType);
              const btnIsInTab = selections.some(({definitionId}) => definitionId === selectedBtn.definitionId);
              if (tab.singleSelectionOnly && btnIsInTab) {
                return handleSingleSelection(
                  tab,
                  selections,
                  selectedBtn,
                  selectionsType,
                  selectionsValue,
                )
              }
              return handleMultiSelection(
                tab,
                selections,
                selectedBtn,
                selectionsType,
                selectionsValue,
              )
            }),
          };
        }),
      };
    });
    updateExpandedMarkets(updatedStore);
  }

  function marketIsFirstExpanded(market) {
    return !market.tabs.some(tab => {
      const {selectionsValue, selectionsType} = getSelectionsWithDifferentTypes(tab.aggregateSelections);
      const selection = collectSelectionsFromDifferentStructures(selectionsValue, selectionsType);
      return selection.some(({priceNum}) => priceNum);
    });
  }

  function handleExpandedMarket(marketDisplayName, isExpanded) {
    const updatedStore = store.map((navigationTab) => {
      if (navigationTab.id === currentNavigationTabId) {
        return {
          ...navigationTab,
          markets: navigationTab.markets.map((market) => {
            if (market.displayName === marketDisplayName) {
              const firstOpen = marketIsFirstExpanded(market);
              return {
                ...market, isExpanded, tabs: market.tabs.map((tab, i) => {
                  if (firstOpen && i === 0) return {...tab, isExpanded: true};
                  return tab;
                })
              };
            }
            return market;
          }),
        };
      }
      return navigationTab;
    });
    if (isExpanded) updateExpandedMarkets(
      updatedStore,
      false,
      false,
      marketDisplayName);
    else setAppData({...appData, store: updatedStore});
  }

  function clearSelections() {
    const updatedStore = store.map(navigationTab => ({
      ...navigationTab,
      markets: navigationTab.markets.map(market => ({
        ...market,
        tabs: market.tabs.map(tab => {
          const {selectionsValue, selectionsType} = getSelectionsWithDifferentTypes(tab.aggregateSelections);
          const tabSelections = collectSelectionsFromDifferentStructures(selectionsValue, selectionsType);
          const selections = tabSelections.map(selection => ({...selection, isSelected: false}));
          return {
            ...tab,
            aggregateSelections: {
              ...tab.aggregateSelections,
              [selectionsType]: restorePreviousSelectionsStructure(
                selectionsValue,
                selectionsType,
                selections
              ),
            }
          }
        })
      }))
    }));
    updateExpandedMarkets(updatedStore);
  }

  const changeNavTab = (selectedNavigationTabId) => {
    if (currentNavigationTabId !== selectedNavigationTabId) {
      setAppData({...appData, isDataUpdating: true});
      updateExpandedMarkets(store, selectedNavigationTabId);
    }
  };

  function postBetPlace(selectedSelections, priceNum = null, priceDen = null, countStake) {
    placeBet(
      fixtureId, selectedSelections, priceNum, priceDen, countStake,
    ).then();
  }

  const goBack = description => changeId(2, competition, leaguesData, description);
  const setCountStake = countStake => setAppData({...appData, countStake});
  const resetApp = () => setAppData({...appData, store: oldStore});
  const {selectedSelections} = collectExpandedSelections(store, currentNavigationTabId);

  if (store.length === 0) return <Preloader/>;

  return (
    <MarketsContext.Provider value={{setBtnSelected, selectedSelections, updateExpandedMarkets}}>
      <AppHeader title={title} leaguesData={leaguesData} goBack={goBack}/>
      <div className={`${style.app} noclick`}>
        <Navigation currentNavigationTabId={currentNavigationTabId} store={store} changeNavTab={changeNavTab}/>
        <div className={style.wrap}>
          {isDataUpdating ? <Preloader/> : (
            <Wrap
              currentNavigationTabId={currentNavigationTabId}
              fixtureId={fixtureId}
              store={store}
              teamNames={teamNames}
              getTabSelectionData={getTabSelectionData}
              updateExpandedMarkets={updateExpandedMarkets}
              handleExpandedMarket={handleExpandedMarket}
              getSelectionsWithDifferentTypes={getSelectionsWithDifferentTypes}
              isAwayTeamDisplayedFirst={isAwayTeamDisplayedFirst}
            />)
          }
        </div>
        {countingDenumerator !== null && selectedSelections.values.length > 0 && (
          <Footer
            resetApp={resetApp}
            store={store}
            countingDenumerator={countingDenumerator}
            selectedSelections={selectedSelections.values}
            teamNames={teamNames}
            countStake={countStake}
            collectExpandedSelections={collectExpandedSelections}
            setBtnSelected={setBtnSelected}
            setCountStake={setCountStake}
            resetStakeSlip={clearSelections}
            postBetPlace={postBetPlace}
          />
        )}
      </div>
    </MarketsContext.Provider>
  );
}
