import React, {useEffect, useState} from 'react';
import _ from "lodash";
import style from "./sportSelector.scss";
import WrapCollapse from "./wrapCollapse";
import {getSportTypes} from "../../api/const";

export default function SportContainer({changeId}) {
  const [sports, setSports] = useState([]);
  useEffect(() => {
    getSportTypes().then(data => (
      setSports(_.orderBy(data, 'displayOrder', 'asc'))
    ))
  }, []);
  return (
    <div className={style.mainMenu}>
      {sports.map((element, index) => <WrapCollapse key={index}  element={element} changeId={changeId}/>)}
    </div>
  )
}
