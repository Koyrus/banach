import React, {Fragment, memo} from 'react';
import style from './sportSelector.scss'
import moment from 'moment'
import {Col, Row} from "reactstrap";

const SelectCompetition = ({
  sortedData, description, changeTemplate, leaguesData, backToMain,
}) => (
  <Fragment>
    <div className={style.backButton} onClick={() => backToMain(1)}>
      <span><i className="fas fa-arrow-left"/></span>
      <span>{leaguesData.description || description.description}</span>
    </div>
    {sortedData.map((item, index) => (
      <div key={index}>
        <div className={style.date}>
          <p>{(moment(item.date).format('dddd D'))}</p>
        </div>
        {item.values.map((el, index) => (
          <div key={index}>
            <div
              className={style.titles}
              onClick={() => changeTemplate(3, el)}>
              <Row className="button-gutters">
                <Col xs={10}>
                  <p>{(moment(el.date).format('HH:mm'))}</p>
                  <p>{el.title}</p>
                </Col>
                <Col xs={2}><i className="fa fa-angle-right"/></Col>
              </Row>
            </div>
          </div>
        ))}
      </div>
    ))}
  </Fragment>
);

export default memo(SelectCompetition, (prevProps, {leaguesData, description: {description}}) => {
  return prevProps.leaguesData.description === leaguesData.description
    || (prevProps.description !== null && prevProps.description.description === description);
});
