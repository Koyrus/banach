import React, {useEffect, useState} from 'react';
import {Collapse} from 'reactstrap';
import style from './sportSelector.scss'
import CollapseContainer from './CompetitionsContainer';
import {getCompetitionsBySportAndCountry} from '../../api/const';

export default function({name, typeId, changeId, element, sportId}) {
  const [collapse, setCollapse] = useState(false);
  const [competitions, setCompetitions] = useState(null);

  useEffect(() => {
    getCompetitionsBySportAndCountry(sportId, typeId).then(competitions => setCompetitions(competitions));
  }, []);

  if (typeId === 'No Country') {
    return <CollapseContainer noCountry element={element} competitions={competitions} changeId={changeId} />;
  }

  return (
    <div className={style.item}>
      <div className={style.title} onClick={() => setCollapse(!collapse)}>
        <div style={{alignSelf: 'flex-end'}}>
          <span style={{paddingLeft: '50px'}}>{name}</span>
        </div>
        <span className={style.icon}>
          <i className={`fa fa-angle-${collapse ? 'up' : 'down'}`} />
        </span>
      </div>
      <Collapse isOpen={collapse}>
        <CollapseContainer element={element} competitions={competitions} changeId={changeId} />
      </Collapse>
    </div>
  );
}
