import React from 'react';
import Preloader from "../preloader/preloader";
import CountryContainer from "./CountryContainer";

export default function ({countries, element, changeId, sportId}) {
  if (countries !== null && countries.length === 0) return <Preloader/>;

  if (countries === null) return <div style={{height: '50px'}} />;

  return countries.sort((a, b) => a.displayOrder - b.displayOrder).map(({typeId, description}) => (
    <CountryContainer
      key={typeId}
      name={description}
      typeId={typeId}
      element={element}
      changeId={changeId}
      sportId={sportId}
    />
  ))
}
