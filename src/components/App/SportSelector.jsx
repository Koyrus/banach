import React, {Fragment, useState} from 'react';
import _ from "lodash";
import moment from 'moment'
import {fixturesByCompetition} from "../../api/const";
import Header from "../header/header";
import App from "./App";
import SelectCompetition from "./selectCompetition";
import SportContainer from './SportContainer';

export default function SportSelector() {
  const [sportSelector, setSportSelector] = useState({
    templateId: 1,
    leaguesData: null,
    competition: null,
    description: null,
    selectedSport: null,
    competitionsByData: [],
  });
  const {
    templateId, leaguesData, competition, description, selectedSport, competitionsByData,
  } = sportSelector;
  const changeTemplate = (id, value) => setSportSelector({...sportSelector, templateId: id, competition: value});
  const changeId = (id, data, desc, sport) => fixturesByCompetition(data.typeId).then(competitions => {
    setSportSelector({
      ...sportSelector,
      templateId: id,
      competitionsByData: competitions,
      leaguesData: data,
      description: desc,
      selectedSport: sport,
    });
  });
  const sortCompetitionData = () => _(competitionsByData)
    .groupBy(x => moment(x.date).format('YYYY-MM-DD'))
    .map((value, key) => ({date: key, values: value}))
    .value();

  function createTemplates() {
    switch (templateId) {
      case 2:
        return (
          <SelectCompetition
            changeId={changeId}
            leaguesData={leaguesData}
            sortedData={sortCompetitionData(competitionsByData)}
            changeTemplate={changeTemplate}
            backToMain={changeTemplate}
            description={description}
          />
        );
      case 3:
        return (
          <App
            changeId={changeId}
            competition={competition}
            leaguesData={leaguesData}
            description={description}
            selectedSport={selectedSport}
          />
        );
      default:
        return <SportContainer changeId={changeId}/>
    }
  }

  return (
    <Fragment>
      <Header/>
      {createTemplates()}
    </Fragment>
  );
}
