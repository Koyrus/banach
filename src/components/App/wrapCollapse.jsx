import React, {memo, useEffect, useState} from 'react';
import {Collapse} from "reactstrap";
import style from './sportSelector.scss'
import {getCountries} from "../../api/const";
import CollapseCountries from "./CollapseCountries";

function WrapCollapse({element, changeId}) {
  const [collapse, setCollapse] = useState(false);
  const [countries, setCountries] = useState(null);
  const {imageUrl, name, sportId} = element;

  useEffect(() => {
    getCountries(sportId).then(countries => {
      if (countries.length > 0) setCountries(countries);
    });
  }, []);

  return (
    <div className={style.item}>
      <div className={style.title} onClick={() => setCollapse(!collapse)}>
        <div style={{alignSelf: 'flex-end'}}>
          <img className={style.sportTypeImage} src={imageUrl} alt=""/>
          <span style={{paddingLeft: '10px'}}>{name}</span>
        </div>
        <span className={style.icon}>
          <i className={`fa fa-angle-${collapse ? 'up' : 'down'}`} />
        </span>
      </div>
      <Collapse isOpen={collapse}>
        <CollapseCountries element={element} countries={countries} changeId={changeId} sportId={sportId} />
      </Collapse>
    </div>
  )
}

export default memo(WrapCollapse, (prevProps, {element: {sportId}}) => prevProps.element.sportId === sportId)
