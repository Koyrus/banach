import React from 'react';
import style from './sportSelector.scss'
import Preloader from "../preloader/preloader";

export default function({competitions, changeId, element, noCountry}) {
  if (competitions !== null && competitions.length === 0) return <Preloader/>;

  return (
    <div
      className={`${style.competitionName} ${noCountry ? style.noCountry : ''}`}
      style={{height: competitions === null ? '50px' : ''}}
    >
      {competitions !== null && competitions.sort((a, b) => a.displayOrder - b.displayOrder).map((item, i) => (
        <p key={i} onClick={() => changeId(2, item, null, element)}>
          {item.description}
          <i className="fa fa-angle-right"/>
        </p>
      ))}
    </div>
  );
}
