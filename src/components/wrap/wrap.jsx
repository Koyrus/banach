import React from 'react';
import style from './wrap.scss'
import {Collapse} from 'reactstrap';
import Templates from '../templates/templates';

export default function Wrap(props) {
  const {store, currentNavigationTabId, handleExpandedMarket, getSelectionsWithDifferentTypes} = props;
  const markets = store.filter(({id}) => currentNavigationTabId === id)[0].markets;
  return markets.sort((a, b) => a.displayOrder - b.displayOrder).map((market, i) => {
    const {id, isExpanded = false, displayName, tabs} = market;
    const foundedTabs = tabs.filter(({isExpanded}) => isExpanded);
    const currentTab = foundedTabs.length > 0 ? foundedTabs[0] : tabs[0];
    const {selectionsValue} = getSelectionsWithDifferentTypes(currentTab.aggregateSelections);
    return (
      <div key={`${id}-${i}`} className={style.wrap}>
        <div className={style.collapseContainer}>
          <div className={style.item} onClick={() => handleExpandedMarket(displayName, !isExpanded)}>
            <div className={style.title}>
              <span>{displayName}</span>
              <span className={style.icon}>
                <i className={`fa fa-angle-${isExpanded ? 'up' : 'down'}`}/>
              </span>
            </div>
          </div>
          <Collapse isOpen={isExpanded}>
            <div className="content-item">
              <div className={style.container}>
                <Templates
                  {...props}
                  market={market}
                  currentTab={currentTab}
                  tabSelections={selectionsValue}
                  tabs={tabs}
                  template={currentTab.template}
                />
              </div>
            </div>
          </Collapse>
        </div>
      </div>
    )
  });
}
