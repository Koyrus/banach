import React, {useEffect, useRef, useState} from "react";
import style from "./menu.scss";
import menuStyle from "./menu.scss";
import $ from "jquery";
import "jquery.kinetic";

export default function Menu(props) {
  const {
    tabs, currentTab, getTabSelectionData, market: {displayName}, correctScore,
  } = props;
  const getTabIndex = () => {
    let tabIndex = 0;
    tabs.forEach(({ id, isExpanded }, i) => {
      if (isExpanded && currentTab !== null && id === currentTab.id) tabIndex = i;
    });
    return tabIndex;
  };
  const selectedTabIndex = getTabIndex();
  const [tabUnderscoreCss, setTabUnderscoreCss] = useState({
    position: 'absolute',
    bottom: 0,
    transition: 'all 0.25s linear',
    height: '4px',
    backgroundColor: '#5A7F26',
    transform: `translateX(${100 * selectedTabIndex}%)`,
    width: `${100 / calcTabWidth(tabs.length)}%`
  });
  const menuRef = useRef(null);

  function calcTabWidth() {
    switch (tabs.length) {
      case 1:
        return '1';
      case 2:
        return '2';
      case 3:
        return '3.05';
      default:
        return '3.5';
    }
  }

  useEffect(() => {
    setTabUnderscoreCss({
      position: 'absolute',
      bottom: 0,
      transition: 'all 0.25s linear',
      height: '4px',
      backgroundColor: '#5A7F26',
      transform: `translateX(${100 * selectedTabIndex}%)`,
      width: `${100 / calcTabWidth(tabs.length)}%`
    })
  }, [tabs.length]);

  useEffect(() => {
    $(".menuRef").kinetic({
      filterTarget({tagName}, {type}) {
        if (!/down|start/.test(type)) return !/area|a|input|div/i.test(tagName);
      }
    });
    if (menuRef.current !== null) menuRef.current.scrollLeft = 100 * selectedTabIndex;
  }, []);

  function menuClick({target: {clientWidth, parentNode}}, selectedTab, tabPosition) {
    if (currentTab !== null && selectedTab.id !== currentTab.id) {
      const newTabUnderscoreCss = {
        ...tabUnderscoreCss,
        transform: `translateX(${tabPosition * clientWidth}px)`,
        width: clientWidth
      };

      $(parentNode).animate(
        {scrollLeft: (clientWidth / 1.4) * tabPosition},
        200,
        () => {
          setTabUnderscoreCss(newTabUnderscoreCss);
          getTabSelectionData(selectedTab, displayName, correctScore);
        }
      );
    }
  }

  function createMenuElement(index, tab) {
    const {displayName, subNavLinkName} = tab;
    return (
      <div
        key={index}
        className={subNavLinkName !== "Outscore the Opposition" ? style.item : style.bigItem}
        style={{
          color: tab.id === currentTab.id ? "#5A7F26" : "#fff",
          fontWeight: tab.id === currentTab.id ? "900" : "600",
          width: `calc(100% / ${calcTabWidth()}`,
          fontSize: `${calcTabWidth() === "1" ? "13px" : "12px"}`
        }}
        onClick={event => menuClick(event, tab, index)}
      >
        {subNavLinkName !== null ? subNavLinkName : displayName}
      </div>
    )
  }

  return tabs.length > 1 && (
    <div className={`${menuStyle.menu} menuRef`} ref={menuRef}>
      {tabs.sort((a, b) => a.subNavLinkOrder - b.subNavLinkOrder).map((tab, i) => createMenuElement(i, tab))}
      <div style={tabUnderscoreCss}/>
    </div>
  )
}
