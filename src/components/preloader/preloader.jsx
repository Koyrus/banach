import React from 'react';
import style from './preloader.scss'

const Preloader = () => (
  <section className={style.preloaderContainer}>
    <div className={style.preloader}>
      <div className={style.spinner}>
        <div className={style.bounce1} />
        <div className={style.bounce2} />
        <div />
      </div>
    </div>
  </section>
);

export default Preloader;
