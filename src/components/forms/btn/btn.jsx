import React from 'react';
import style from './btn.scss'

export default function Btn(props) {
  const {
    onClick, isSelected, text, isBig, handicap, handicapValue, className = '', responseCode,
  } = props;
  const isDisabled = responseCode === 2 || text === '0 / 0' || text === '0/0';

  return (
    <button
      className={
        `${style.button} : ${isSelected ? style.selected : ''}
        ${isDisabled ? style.disabled : ''}
        ${isBig ? style.big : ''} ${className}`}
      onClick={() => isDisabled ? false : onClick()}
      style={{minWidth: '70px', width: '100%'}}
    >
      <div className={handicap ? style.button_body : style.button_text}>
        {handicap && handicapValue && (
          <div className={style.newHandicapButton}>
            {`${handicapValue < 0 ? '' : '+'}${handicapValue * 1}`}
          </div>
        )}
        {isDisabled ? '-' : text}
      </div>
    </button>
  )
}

export const Div = ({isSelected, text, isBig, className = ''}) => (
  <div
    className={`${style.sameButton} : ${isSelected ? style.selected : ''}  ${isBig ? style.big : ''} ${className}`}
  >
    {text}
  </div>
);
