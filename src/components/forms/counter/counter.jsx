import React from 'react';
import style from './counter.scss';

export default function Counter({min, max, type, handleScore, counter}) {
  const increment = () => handleScore(type, counter >= max ? max : counter + 1);
  const decrement = () => handleScore(type, counter <= min ? min : counter - 1);
  return (
    <div className={style.counter}>
      <span className={`${style.btn} ${style.leftSide}`} onClick={decrement}>
         <i className="fa fa-minus"/>
      </span>
      <div className={style.counter__center}>{counter}</div>
      <span className={`${style.btn} ${style.rightSide}`} onClick={increment}>
         <i className="fa fa-plus"/>
      </span>
    </div>
  )
}
