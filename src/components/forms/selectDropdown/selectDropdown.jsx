import React, {useContext} from 'react';
import {Col, Row} from 'reactstrap';
import Btn from '../btn/btn';
import MarketsContext from '../../../contexts/MarketsContext';
import styles from './selectDropdown.scss';
import {getClosestSelectionForOptimalLine} from '../../utils';

export default function SelectDropdown({selections, defaultSelection}) {
  const {setBtnSelected, updateExpandedMarkets} = useContext(MarketsContext);
  const defaultOption = selections.filter(selection => selection.totalsLine === defaultSelection)[0]
    || getClosestSelectionForOptimalLine(defaultSelection, selections);
  const option = selections.filter(selection => selection.visibleSelection)[0];
  const visibleOption = option ? option : defaultOption;
  const {priceNum = 0, priceDen = 0, hundredPcLine, responseCode} = visibleOption;

  return (
    <Row className="button-gutters">
      <Col xs={6} md={6}>
        <select
          className={styles.select}
          value={option ? option.definitionId : visibleOption.definitionId}
          onChange={({target: {value}}) => {
            const selectedSelection = selections.filter(({definitionId}) => definitionId === Number(value))[0];
            if (selectedSelection) updateExpandedMarkets(undefined, undefined, undefined, undefined, selectedSelection);
          }}>
          {selections.map(({definitionId, totalsLine}) => (
            <option key={definitionId} value={definitionId}>{`${totalsLine} +`}</option>
          ))}
        </select>
      </Col>
      <Col xs={6} md={6}>
        <Btn
          text={`${priceNum} / ${priceDen}`}
          isSelected={visibleOption.isSelected}
          hundredPcLine={hundredPcLine}
          onClick={() => setBtnSelected(visibleOption)}
          responseCode={responseCode}
        />
      </Col>
    </Row>
  )
}
