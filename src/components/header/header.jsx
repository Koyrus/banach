import React from 'react';
import style from './header.scss'

const Header = () => (
  <div className={style.header}>
    <figure>
      <img src={process.env.PUBLIC_URL + '/assets/images/logo-v2.png'} alt=""/>
    </figure>
  </div>
);

export default Header;
