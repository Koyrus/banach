import React, {Fragment, useContext, useEffect, useState} from 'react';
import {Button, Col, Collapse, Row} from 'reactstrap';
import SetStake from './setStake/setStake';
import BetSlip from './betSlip/betslip';
import style from './footer.scss'
import MarketsContext from '../../contexts/MarketsContext';

export default function Footer(props) {
  const {
    countingDenumerator, resetApp, teamNames: {homeTeam, awayTeam}, setCountStake, countStake, selectedSelections,
    resetStakeSlip, store,
  } = props;
  const {priceNum, priceDen, responseMessage} = countingDenumerator;
  const {setBtnSelected} = useContext(MarketsContext);
  const [footerData, setFooterData] = useState({
    collapseStake: false,
    count: 0,
    collapse: false,
    hideWhenBetSlip: false,
    showError: false,
    showErrorCount: false,
  });
  const {
    collapseStake, count, collapse, hideWhenBetSlip, showError, showErrorCount,
  } = footerData;
  console.log(selectedSelections)
  const toggleStake = () => setFooterData({...footerData, collapseStake: !collapseStake});
  const toggle = () => setFooterData({...footerData, collapse: !collapse});
  const reuseSelections = () => setFooterData({...footerData, hideWhenBetSlip: false, count: 0});

  useEffect(() => setFooterData({...footerData, showError: false}), [showError && selectedSelections.length > 1]);
  useEffect(() => setFooterData({...footerData, showErrorCount: false}), [showErrorCount && countStake > 0]);

  function showBetSlip() {
    if (selectedSelections.length < 2) {
      if (countStake === 0 || countStake === '') {
        return setFooterData({...footerData, showErrorCount: true, showError: true});
      }
      return setFooterData({...footerData, showError: true});
    } else if (countStake === 0 || countStake === '') {
      return setFooterData({...footerData, showErrorCount: true, showError: false});
    }
    return setFooterData({...footerData, hideWhenBetSlip: true, showErrorCount: false, showError: false});
  }

  function createStep(selection,) {
    const {definitionId, marketName = null, tabName = null, betSlipName, displayName , tabNameForYesNo} = selection;
    return (
      <Row className={style.listItem} key={definitionId}>
        <Col xs={2}>
          <div className={style.leftSide}>
            <div className={style.iconarea}>
              <i className={`${style.iconfa} fa fa-chevron-circle-down`}/>
            </div>
          </div>
        </Col>
        <Col xs={8}>
          <p className={style.selectedName}>{betSlipName || displayName} </p>
          <span className={style.selectedMarketName}>
            {`${`${marketName !== null ? marketName : ''}
            ${marketName !== null && tabName !== null ? '-' : '-'}
            ${tabName !== null && tabName !== undefined ? tabName : tabNameForYesNo}`}`}
          </span>
        </Col>
        <Col xs={2} className="pt-2">
          <div onClick={() => setBtnSelected(selection)}>
            <i className={`${style.iconRemove} fa fa-times`} aria-hidden="true"/>
          </div>
        </Col>
      </Row>
    )
  }

  return (
    <Fragment>
      {!hideWhenBetSlip && (
        <div className={style.footer}>
          {responseMessage !== null && (
            <Row className={`${style.errorMessage}`}>
              <Col xs={2} md={2}>
                <i className="fas fa-exclamation-triangle"/>
              </Col>
              <Col xs={10} md={10}>
                <span>The price for this selection is unavailable please try another combination</span>
              </Col>
            </Row>
          )}
          {showError && (
            <Row className={`${style.errorMessage}`}>
              <Col xs={1} md={1}>
                <i className="fas fa-exclamation-triangle"/>
              </Col>
              <Col xs={10} md={10}>
                <span>A minimum of 2 selections must be added</span>
              </Col>
            </Row>
          )}
          {showErrorCount && (
            <Row className={`${style.errorMessage}`}>
              <Col xs={1} md={1}>
                <i className="fas fa-exclamation-triangle"/>
              </Col>
              <Col xs={10} md={10}>
                <span>Please enter Stake</span>
              </Col>
            </Row>
          )}
          <div className={priceNum && priceDen ? style.headerUp : style.headerDown} onClick={toggle}>
            <span>{homeTeam.name} vs {awayTeam.name}</span>
            <span className={style.right}>
                &nbsp;
              <span style={{paddingRight: '10px'}}>
                  <span>{priceNum} / {priceDen}</span>
                </span>
                <span className={style.icon}><i className={`fa fa-angle-${collapse ? 'up' : 'down'}`}/></span>
              </span>
          </div>
          <Collapse isOpen={collapse}>
            <div className={style.FooterList}>
              {selectedSelections.map(selection => createStep(selection, setBtnSelected))}
            </div>
            <Fragment>
              <SetStake
                countingDenumerator={countingDenumerator}
                toggleStake={toggleStake}
                setCountStake={setCountStake}
              />
              <div className={style.setStakeCont}>
                <Button
                  className={style.setStakeButton}
                  onClick={() => showBetSlip(selectedSelections, countStake)}
                  disabled={responseMessage !== null}
                >
                  Place Bet
                </Button>
              </div>
              <div className={style.clearBetslip} onClick={() => resetStakeSlip(store)}>
                <span>Clear Betslip</span>
              </div>
            </Fragment>
          </Collapse>
        </div>
      )}
      {hideWhenBetSlip && (
        <BetSlip
          resetApp={resetApp}
          countingDenumerator={countingDenumerator}
          count={count}
          reuseSelections={reuseSelections}
          selectedSelections={selectedSelections}
          countStake={countStake}/>
      )}
    </Fragment>
  );
}
