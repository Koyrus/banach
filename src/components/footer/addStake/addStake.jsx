import React, {useState} from 'react';
import {Col, Collapse, Row} from 'reactstrap';
import style from './addStake.scss'
import {strings} from '../../../lang/const';

export default function AddStake({hideSetAddStake, getStakeCount, countingDenumerator: { priceNum, priceDen}, closeAddStake}) {
  const [collapse, setCollapse] = useState(true);
  const [count, setCount] = useState('');
  const toggle = () => setCollapse(!collapse);
  const handleSet = item => setCount(String(count + item));
  const plusOperation = addValue => setCount(Number(count) + addValue);
  const handleChange = ({target: {value}}) => setCount(Number(value));

  function resetForm() {
    if (typeof count === 'string') setCount(count.substring(0, count.length - 1));
    else setCount(count);
  }

  function placeBet() {
    hideSetAddStake(true);
    getStakeCount(count)
  }

  return (
    <div className="collapse-container">
      <div className={style.item} onClick={toggle}>
        <div className={style.title}>
          <span>  </span>
          <span className={style.icon}>
            <i className={`fa fa-angle-${collapse ? 'up' : 'down'}`}/>
          </span>
        </div>
      </div>
      <Collapse isOpen={collapse}>
        <div style={{background: 'white'}}>
          <div className={style.container}>
            <div className={style.inputArea}>
              <span className={style.stakeLabel}>{strings.stake}</span>
              <input type="number" value={count} onChange={handleChange} placeholder={'£'} />
            </div>
            <Row className="button-gutters-small">
              <Col xs={3}>
                <button className={style.buttonDark} onClick={() => plusOperation(5)}>+5</button>
                <button className={style.buttonLight} onClick={() => handleSet('7')}>7</button>
                <button className={style.buttonLight} onClick={() => handleSet('4')}>4</button>
                <button className={style.buttonLight} onClick={() => handleSet('1')}>1</button>
                <button className={style.buttonLight} onClick={() => handleSet('00')}>00</button>
              </Col>
              <Col xs={3}>
                <button className={style.buttonDark} onClick={() => plusOperation(10)}>+10</button>
                <button className={style.buttonLight} onClick={() => handleSet('8')}>8</button>
                <button className={style.buttonLight} onClick={() => handleSet('5')}>5</button>
                <button className={style.buttonLight} onClick={() => handleSet('2')}>2</button>
                <button className={style.buttonLight} onClick={() => handleSet('0')}>0</button>
              </Col>
              <Col xs={3}>
                <button className={style.buttonDark} onClick={() => plusOperation(15)}>+15</button>
                <button className={style.buttonLight} onClick={() => handleSet('9')}>9</button>
                <button className={style.buttonLight} onClick={() => handleSet('6')}>6</button>
                <button className={style.buttonLight} onClick={() => handleSet('3')}>3</button>
                <button className={style.buttonLight} disabled={toString(count).includes('.')}
                        onClick={() => handleSet('.')}>.
                </button>
              </Col>
              <Col xs={3}>
                <button className={style.buttonDark} onClick={() => plusOperation(50)}>+50</button>
                <button className={style.buttonLightBig} onClick={() => resetForm()}>
                  <i className="fas fa-backspace"/>
                </button>
                <button className={style.buttonLightBig}>
                  {strings.ok}
                </button>
              </Col>
            </Row>
          </div>
          <div className={style.bets}>
            <Row>
              <Col xs={6}>{count !== '' ? <span>Stake £{count}</span> : <span>{strings.stake} £0</span>}</Col>
              <Col xs={6} className="d-flex justify-content-end align-items-end">
                Est. Returns £{((priceNum / priceDen + 1) * count).toFixed(2)}
              </Col>
            </Row>
          </div>
          <div className={style.bottom}>
            <Row>
              <Col xs={2} onClick={closeAddStake}>
              </Col>
              <Col xs={10} className="d-flex justify-content-end align-items-end">
              </Col>
            </Row>
          </div>
        </div>
      </Collapse>
    </div>
  )
}
