import React, {useEffect, useState} from 'react';
import {Col, Collapse, Row} from 'reactstrap'
import $ from "jquery";
import {strings} from './../../../lang/const'
import style from './setStake.scss';

export default function SetStake({toggleStake, setCountStake, countingDenumerator: {priceNum, priceDen}, amount}) {
  const [collapse, setCollapse] = useState(false);
  const [count, setCount] = useState('');
  const toggle = () => setCollapse(!collapse);
  const handleSet = item => setCount('' + count + item);
  const handleChange = ({target: {value}}) => setCount(value);
  const plusOperation = addValue => setCount(Number(count) + addValue);

  useEffect(() => toggleStake(!collapse), [collapse]);
  useEffect(() => setCountStake(count), [count]);

  function resetForm() {
    const value = String(count);
    setCount(value.substring(0, value.length - 1));
  }

  return (
    <div className="collapse-container">
      <div className={style.item}>
        {amount < count && (
          <Row className={`${style.errorMessage}`}>
            <Col xs={2} md={2}>
              <i className="fas fa-exclamation-triangle"/>
            </Col>
            <Col xs={10} md={10}>
              <span>You need to add funds to your account in  order to place this bet of £{count}</span>
            </Col>
          </Row>
        )}
        <Row onClick={toggle}>
          <Col xs={6} md={6}>
            <div className={style.title}>
              <span>Set Stake</span>
            </div>
          </Col>
          <Col xs={4} md={4}>
            <div className={style.inputArea}>
              <input
                readOnly={$(document).width() < 415}
                type="number"
                value={count}
                onChange={handleChange}
                placeholder='£'
              />
            </div>
          </Col>
          <Col xs={2} md={2}>
            <span className={style.icon}><i className={`fa fa-angle-${collapse ? 'up' : 'down'}`}/></span>
          </Col>
        </Row>
      </div>
      <Collapse isOpen={collapse}>
        <div className={style.returns}>
          <p>Est. Returns: £ {((priceNum / priceDen + 1) * count).toFixed(2)}</p>
        </div>
        <div className={style.calcComponent}>
          <Row className="button-gutters-small">
            <Col xs={3}>
              <button className={style.buttonDark} onClick={() => plusOperation(5)} onTouchStart={() => {
              }}>+5
              </button>
              <button className={style.buttonLight} onClick={() => handleSet('7')} onTouchStart={() => {
              }}>7
              </button>
              <button className={style.buttonLight} onClick={() => handleSet('4')} onTouchStart={() => {
              }}>4
              </button>
              <button className={style.buttonLight} onClick={() => handleSet('1')} onTouchStart={() => {
              }}>1
              </button>
              <button className={style.buttonLight} onClick={() => handleSet('00')} onTouchStart={() => {
              }}>00
              </button>
            </Col>
            <Col xs={3}>
              <button className={style.buttonDark} onClick={() => plusOperation(10)} onTouchStart={() => {
              }}>+10
              </button>
              <button className={style.buttonLight} onClick={() => handleSet('8')} onTouchStart={() => {
              }}>8
              </button>
              <button className={style.buttonLight} onClick={() => handleSet('5')} onTouchStart={() => {
              }}>5
              </button>
              <button className={style.buttonLight} onClick={() => handleSet('2')} onTouchStart={() => {
              }}>2
              </button>
              <button className={style.buttonLight} onClick={() => handleSet('0')} onTouchStart={() => {
              }}>0
              </button>
            </Col>
            <Col xs={3}>
              <button className={style.buttonDark} onClick={() => plusOperation(15)} onTouchStart={() => {
              }}>+15
              </button>
              <button className={style.buttonLight} onClick={() => handleSet('9')} onTouchStart={() => {
              }}>9
              </button>
              <button className={style.buttonLight} onClick={() => handleSet('6')} onTouchStart={() => {
              }}>6
              </button>
              <button className={style.buttonLight} onClick={() => handleSet('3')} onTouchStart={() => {
              }}>3
              </button>
              <button className={style.buttonLight} onClick={() => handleSet('.')} onTouchStart={() => {
              }}>.
              </button>
            </Col>
            <Col xs={3}>
              <button className={style.buttonDark} onClick={() => plusOperation(50)} onTouchStart={() => {
              }}>+50
              </button>
              <button className={style.buttonLightBig} onClick={() => resetForm()} onTouchStart={() => {
              }}>
                <i className="fas fa-backspace" style={{fontSize: '20px'}}/>
              </button>
              <button className={style.buttonLightBig} onClick={toggle}>
                {strings.ok}
              </button>
            </Col>
          </Row>
        </div>
      </Collapse>
    </div>
  )
}
