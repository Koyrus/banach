import React from 'react'
import {Button, Col, Row} from 'reactstrap';
import {strings} from '../../../lang/const';
import style from './betslip.scss'

export default function BetSlip({countingDenumerator, selectedSelections, resetApp, countStake, reuseSelections}) {
  const {priceNum, priceDen, responseMessage} = countingDenumerator;
  return (
    <div className={style.betSlip}>
      <div className={style.sticked}>
        <div className={style.receipt}>{strings.betReceipt}</div>
        <div className={style.content}>
          {selectedSelections.map((selection, index) => {
            const {displayName, marketName = null, tabName = null , tabNameForYesNo} = selection;
            return (
              <Row className="button-gutters pb-1" key={index}>
                <Col xs={12} md={12}>
                  <Row
                    key={index}
                    className={responseMessage === null ? style.listItem : `${style.listItem} ${style.Error}`}
                  >
                    <Col xs={2}>
                      <div className={style.leftSide}>
                        <div className={style.iconarea}>
                          <i className={`${style.iconfa} fa fa-chevron-circle-down`}/>
                        </div>
                      </div>
                    </Col>
                    <Col xs={10}>
                      <p className={style.selectedName}>{displayName} </p>
                      <span className={style.selectedMarketName}>
                                 {`${`${marketName !== null ? marketName : ''}
            ${marketName !== null && tabName !== null ? '-' : '-'}
            ${tabName !== null && tabName !== undefined ? tabName : tabNameForYesNo}`}`}
                    </span>
                    </Col>
                  </Row>
                </Col>
              </Row>
            )
          })}
          <p className={style.odds}>{`Odds ${priceNum} / ${priceDen}`}</p>
        </div>
        <div className={style.stakeSetted}>
          <Row className={`${style.styledRow} button-gutters`}>
            <Col xs={6}><span className={style.countStake}>{strings.stake} £{countStake}</span></Col>
            <Col xs={6} className={`d-flex justify-content-end align-items-end`}>
            <span className={style.leftCountNumerator}>
               {`${strings.potentialReturns} £${((priceNum / priceDen + 1) * countStake).toFixed(2)}`}
            </span>
            </Col>
          </Row>
        </div>
        <div className={style.stakeUnderSetted}>{strings.successBet}</div>
        <img className={style.bottomImage} src={process.env.PUBLIC_URL + '/assets/images/bottom.jpg'} alt=""/>
        <div className={style.reuseSelection}>
          <Row className="text-center">
            <Col xs={6} md={6}>
              <Button onClick={reuseSelections} className={style.receiptButtons}>{strings.reuseSelections}</Button>
            </Col>
            <Col xs={6} md={6}>
              <Button onClick={resetApp} className={style.receiptButtons}>{strings.betAgain}</Button>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  )
}
