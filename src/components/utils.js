import {Col} from "reactstrap";
import Btn from "./forms/btn/btn";
import React from "react";
import style from "./templates/PlayerStats/playerStats.scss";
import Row from "reactstrap/es/Row";

function createOverUnderSelection(selection = {}, setBtnSelected, colSize) {
  const {
    priceNum = 0, priceDen = 0, hundredPcLine, isSelected, responseCode,
  } = selection;
  return (
    <Col xs={colSize} md={colSize}>
      <Btn
        text={`${priceNum} / ${priceDen}`}
        hundredPcLine={hundredPcLine}
        isSelected={isSelected}
        responseCode={responseCode}
        onClick={() => setBtnSelected(selection)}
      />
    </Col>
  )
}

export function createSelectionsWithNames(
  overSelections,
  underSelections,
  selectionsName,
  setBtnSelected,
  exactlySelection = false,
) {
  const colSize = exactlySelection ? 3 : 4;
  return overSelections.map((selection, i) => (
    <Row key={`${selection.displayName}-${i}`} className="button-gutters mr-1 ml-1">
      <Col xs={colSize} md={colSize}>
        <div className={style.doubleNames}>{selectionsName[i].totalsLine}</div>
      </Col>
      {createOverUnderSelection(selection, setBtnSelected, colSize)}
      {exactlySelection ? createOverUnderSelection(exactlySelection[i], setBtnSelected, colSize) : ''}
      {createOverUnderSelection(underSelections[i], setBtnSelected, colSize)}
    </Row>
  ));
}

export function getUniqueNames(selections) {
  const uniqueNames = [];
  selections.forEach(selection => {
    if (!uniqueNames.some(btn => btn.totalsLine === selection.totalsLine)) uniqueNames.push(selection);
  });
  return uniqueNames;
}

export function filterByColumnName(betSlipName, displayName, type) {
  return (betSlipName !== null && betSlipName.includes(type)) || (displayName !== null && displayName.includes(type))
}

export function getClosestSelectionForOptimalLine(optimalLine, selections) {
  let value = {minValue: Math.abs(optimalLine - selections[0].totalsLine), selection: selections[0]};

  for (let i = 1; i < selections.length; i++) {
    if (value.minValue > Math.abs(optimalLine - selections[i].totalsLine)) {
      value = {minValue: Math.abs(optimalLine - selections[i].totalsLine), selection: selections[i]}
    }
  }

  return value.selection;
}
