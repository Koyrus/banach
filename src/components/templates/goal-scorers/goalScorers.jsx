import React, {Fragment, useContext} from 'react';
import {Col, Row} from 'reactstrap';
import Btn from '../../forms/btn/btn';
import MarketsContext from '../../../contexts/MarketsContext';
import MatchBetting from '../match-betting/matchBetting';
import style from '../../templates/PlayerStats/playerStats.scss'

export default function GoalScorers(props) {
  const {teamNames: {homeTeam, awayTeam}, tabSelections, isAwayTeamDisplayedFirst} = props;
  const {setBtnSelected} = useContext(MarketsContext);

  function divideGoalScoresSelections(selections,) {
    return selections.map((selection, i) => {
      const {
        isSelected, priceNum = 0, priceDen = 0, displayName, responseCode,
      } = selection;
      return (
        <Row className="m-0" key={`${displayName}-${i}`}>
          <Col xs={6} sm={6} md={6} lg={6} xl={6} className="p-0">
            <div className={style.totalsLine}>{displayName}</div>
          </Col>
          <Col xs={6} sm={6} md={6} lg={6} xl={6} className="p-0">
            <Btn
              text={`${priceNum} / ${priceDen}`}
              isSelected={isSelected}
              responseCode={responseCode}
              onClick={() => setBtnSelected(selection)}
            />
          </Col>
        </Row>
      );
    })
  }

  function createGoalScoresSelections() {
    const noGoalScorerSelection = tabSelections.filter(({relatedTeamId}) => relatedTeamId === undefined);
    const sortFunc = (a, b) => a.displayOrder - b.displayOrder;
    const firstColumn = [...tabSelections.filter(({relatedTeamId}) => relatedTeamId === homeTeam.participantId)
      .sort(sortFunc), ...noGoalScorerSelection];
    const secondColumn = tabSelections.filter(({relatedTeamId}) => relatedTeamId === awayTeam.participantId)
      .sort(sortFunc);
    return (
      <Row className="button-gutters pt-3">
        <Col xs={6}>
          {divideGoalScoresSelections(isAwayTeamDisplayedFirst ? secondColumn : firstColumn)}
        </Col>
        <Col xs={6}>
          {divideGoalScoresSelections(isAwayTeamDisplayedFirst ? firstColumn : secondColumn)}
        </Col>
      </Row>
    )
  }

  return (
    <MatchBetting {...props} customMenuBody={(
      <Fragment>
        <Row className="button-gutters 333">
          <Col xs={6} md={6}>
            <span className={style.playerStatNames}>{isAwayTeamDisplayedFirst ? awayTeam.name : homeTeam.name}</span>
          </Col>
          <Col xs={6} md={6}>
            <span className={style.playerStatNames}>{isAwayTeamDisplayedFirst ? homeTeam.name : awayTeam.name}</span>
          </Col>
        </Row>
        {createGoalScoresSelections()}
      </Fragment>
    )}
    />
  )
}
