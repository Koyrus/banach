import React, {useContext} from 'react'
import {Col, Row} from 'reactstrap';
import _ from 'lodash';
import Btn from '../../forms/btn/btn';
import style from '../../templates/PlayerStats/playerStats.scss'
import stylesYes from './yesNoSelection.scss'
import styleFromFooter from '../../footer/footer.scss'
import MarketsContext from '../../../contexts/MarketsContext';

export default function YesNoSelection({tabSelections}) {
  const yesNoSelections = _.orderBy(tabSelections, ['displayOrder'], ['asc']);
  const noSelections = yesNoSelections.map(({noSelection}) => noSelection);
  const yesSelections = yesNoSelections.map(({yesSelection}) => yesSelection);
  const {setBtnSelected} = useContext(MarketsContext);

  function createYesNoSelectionsName() {
    return yesNoSelections.map(({groupedName}, i) => {
      return (
        <Row key={`${groupedName}-${i.toString()}`} className="button-gutters mr-0 ml-0">
          <Col xs={12} md={12} className={`pl-0`}>
            <p align="center" className={style.popularName}>{groupedName}</p>
          </Col>
        </Row>
      )
    });
  }

  function createSelections(selections) {
    return _.orderBy(selections, ['displayOrder'], ['desc']).map((selection = {}, i) => {
      const {
        priceNum = 0, priceDen = 0, hundredPcLine, displayName, responseCode
      } = selection;
      return (
        <Btn
          key={`${displayName}-${i}`}
          text={`${priceNum} / ${priceDen}`}
          isSelected={selection.isSelected}
          hundredPcLine={hundredPcLine}
          responseCode={responseCode}
          onClick={() => setBtnSelected(selection)}
        />
      )
    })
  }

  return (
    <div className={stylesYes.content}>
      <div className={style.yesNoSpace}>
        <Row className='button-gutter'>
          <Col xs={6} md={6}/>
          <Col xs={3} md={3}>
            <p className={style.selections}>Yes</p>
          </Col>
          <Col xs={3} md={3}>
            <p className={style.selections}>No</p>
          </Col>
        </Row>
      </div>
      <div className={`button-gutters mr-0 ${styleFromFooter.popularList}`}>
        <Row className={`button-gutters mr-0 ml-0 ${styleFromFooter.listItem}`}>
          <Col xs={6} md={6} className={`${styleFromFooter.colPopular} pl-0`}>
            {createYesNoSelectionsName()}
          </Col>
          <Col xs={3} md={3} className={`${styleFromFooter.paddingCol} pl-0`}>
            {createSelections(yesSelections)}
          </Col>
          <Col xs={3} md={3} className={`${styleFromFooter.paddingCol} pl-0`}>
            {createSelections(noSelections)}
          </Col>
        </Row>
      </div>
    </div>
  );
}
