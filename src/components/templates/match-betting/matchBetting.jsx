import React, {Fragment, useContext} from 'react';
import {Col, Row} from 'reactstrap';
import _ from 'lodash';
import Menu from '../../menu/menu';
import MarketsContext from '../../../contexts/MarketsContext';
import style from '../PlayerStats/playerStats.scss';
import Btn from '../../forms/btn/btn';

export default function MatchBetting(props) {
  const {tabSelections, customMenuBody, tabs, template, isAwayTeamDisplayedFirst} = props;
  const selections = _.orderBy(
    tabSelections,
    ['displayOrder'],
    [isAwayTeamDisplayedFirst ? 'desc' : 'asc'],
  );
  const {setBtnSelected} = useContext(MarketsContext);
  const colSize = selections.length === 2 || selections.length > 3 ? 6 : 4;
  return (
    <Fragment>
      <Menu {...props}/>
      {customMenuBody ? customMenuBody : (
        <Row className={`button-gutters mr-1 ml-1 ${tabs.length === 1 ? 'pt-2' : ''}`}>
          {selections.map((selection, i) => {
            const {
              displayName, priceNum = 0, priceDen = 0, hundredPcLine, isSelected, responseCode,
            } = selection;

            const btn = (
              <Btn
                text={`${priceNum} / ${priceDen}`}
                hundredPcLine={hundredPcLine}
                isSelected={isSelected}
                responseCode={responseCode}
                onClick={() => setBtnSelected(selection)}
              />
            );

            if ((tabs.length > 1 || template === 11) && template !== 6) {
              return (
                <Col key={`${displayName}-${i}`} xs={colSize} md={colSize}>
                  <Row>
                    <Col xs={12}>
                      <div className={style.totalsLine} style={{textAlign: 'center', paddingLeft: '0'}}>
                        {displayName}
                      </div>
                    </Col>
                    <Col xs={12}>{btn}</Col>
                  </Row>
                </Col>
              );
            }

            return (
              <Fragment key={`${displayName}-${i}`}>
                <Col xs={9} md={9} style={{paddingLeft: '20px'}}>
                  <div className={style.totalsLine} style={{paddingLeft: '0'}}>{displayName}</div>
                </Col>
                <Col xs={3} md={3}>{btn}</Col>
              </Fragment>
            )
          })}
        </Row>
      )}
    </Fragment>
  );
};
