import React from 'react';
import {Col, Row} from "reactstrap";
import SelectDropdown from "../../forms/selectDropdown/selectDropdown";
import style from '../../templates/PlayerStats/playerStats.scss'
import MatchBetting from "../match-betting/matchBetting";

const PlayerStats = props => (
  <div className={style.stats}>
    <MatchBetting {...props} customMenuBody={props.tabSelections.map((select, index) => {
      const {optimalLine, selections, playerName} = select;
      return (
        <Row key={index} className="button-gutters">
          <Col xs={5}>
            <p className={style.playerStatNames}>{playerName}</p>
          </Col>
          <Col xs={7}>
            <SelectDropdown defaultSelection={optimalLine} selections={selections}/>
          </Col>
        </Row>
      )
    })}/>
  </div>
);

export default PlayerStats;
