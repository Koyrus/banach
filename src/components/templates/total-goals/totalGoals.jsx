import React, {Fragment, useContext} from 'react';
import _ from 'lodash';
import {Col, Row} from 'reactstrap';
import MatchBetting from '../match-betting/matchBetting';
import MarketsContext from '../../../contexts/MarketsContext';
import {createSelectionsWithNames, filterByColumnName, getUniqueNames} from '../../utils';
import style from '../PlayerStats/playerStats.scss';

const TotalGoals = props => {
  const {tabSelections, tabs, currentTab} = props;
  const {setBtnSelected} = useContext(MarketsContext);
  const selections = _.orderBy(tabSelections, ['displayOrder'], ['asc']);
  const overSelections = selections.filter(({betSlipName, displayName}) =>
    filterByColumnName(betSlipName, displayName, 'Over'));
  const underSelections = selections.filter(({betSlipName, displayName}) =>
    filterByColumnName(betSlipName, displayName, 'Under'));
  const { homeOverSelectionDisplayName, awayUnderSelectionDisplayName } = currentTab;

  return <MatchBetting {...props} customMenuBody={(
    <Fragment>
      <Row className={`button-gutters mr-1 ml-1${tabs.length < 2 ? ' pt-2' : ''}`}>
        <Col xs={4}/>
        <Col xs={8}>
          <Row className="button-gutters mr-1 ml-1">
            <Col xs={6}>
              <p className={style.overUnderText}>{homeOverSelectionDisplayName}</p>
            </Col>
            <Col xs={6}>
              <p className={style.overUnderText}>{awayUnderSelectionDisplayName}</p>
            </Col>
          </Row>
        </Col>
      </Row>
      {createSelectionsWithNames(
        overSelections,
        underSelections,
        getUniqueNames(selections),
        setBtnSelected)}
    </Fragment>
  )}/>
};

export default TotalGoals;
