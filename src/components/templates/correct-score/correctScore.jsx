import React, {Fragment, useContext, useEffect, useState} from 'react';
import _ from 'lodash';
import {Col, Row} from 'reactstrap';
import Menu from '../../menu/menu';
import MarketsContext from '../../../contexts/MarketsContext';
import mainStyle from './correctScore.scss';
import Counter from '../../forms/counter/counter';
import Btn from '../../forms/btn/btn';

const CorrectScore = props => {
  const [correctScore, setCorrectScore] = useState({awayScore: 0, homeScore: 0});
  const { tabSelections, teamNames: {awayTeam, homeTeam}, isAwayTeamDisplayedFirst} = props;
  const [currentHomeScore, setCurrentHomeScore] = useState(0);
  const [currentAwayScore, setCurrentAwayScore] = useState(0);
  const {min: minHome, max: maxHome} = calcMinMax('homeScore');
  const {min: minAway, max: maxAway} = calcMinMax('awayScore');
  const {setBtnSelected, updateExpandedMarkets} = useContext(MarketsContext);
  const selectedSelection = tabSelections.filter(({awayScore, homeScore}) =>
    Number(awayScore) === currentAwayScore && Number(homeScore) === currentHomeScore)[0] || {};

  useEffect(() => {
    setCorrectScore({awayScore: currentAwayScore, homeScore: currentHomeScore});
  }, [currentAwayScore, currentHomeScore]);

  function calcMinMax(type) {
    const maxTemp = _.maxBy(tabSelections, selection => selection[type]);
    const minTemp = _.minBy(tabSelections, selection => selection[type]);
    return {min: minTemp ? minTemp[type] : '', max: maxTemp ? maxTemp[type] : ''};
  }

  function getSelectedScore() {
    tabSelections.forEach((selection) => {
      const {awayScore, homeScore} = selection;
      if (currentAwayScore === Number(awayScore) && currentHomeScore === Number(homeScore)) {
        return setBtnSelected(selection);
      }
    })
  }

  function handleScore(type, selectedScore) {
    const selection = tabSelections.filter(({awayScore, homeScore}) =>
      Number(awayScore) === (type === 1 ? selectedScore : currentAwayScore)
      && Number(homeScore) === (type === 0 ? selectedScore : currentHomeScore))[0];
    updateExpandedMarkets(undefined, undefined, undefined, undefined, selection);
    return type === 0 ? setCurrentHomeScore(selectedScore) : setCurrentAwayScore(selectedScore);
  }

  function findScoreInArray() {
    const {priceNum = 0, priceDen = 0} = selectedSelection;
    return `${priceNum} / ${priceDen}`;
  }

  function createColumn(title, min, max, counter, type) {
    return (
      <Col xs={6} md={6} className="text-center">
        <div className={mainStyle.correctTitle}>{title}</div>
        <Counter type={type} min={min} max={max} counter={counter} handleScore={handleScore}/>
      </Col>
    )
  }
  return (
    <Fragment>
      <Menu {...props} correctScore={correctScore}/>
      <Fragment>
        <Row className="button-gutters pb-2">
          <Col xs={9}>
            <Row>
              {createColumn(
                isAwayTeamDisplayedFirst ? (awayTeam ? awayTeam.name : '') : (homeTeam ? homeTeam.name : ''),
                isAwayTeamDisplayedFirst ? minAway : minHome,
                isAwayTeamDisplayedFirst ? maxAway : maxHome,
                isAwayTeamDisplayedFirst ? currentAwayScore : currentHomeScore,
                isAwayTeamDisplayedFirst ? 1 : 0,
              )}
              {createColumn(
                isAwayTeamDisplayedFirst ? (homeTeam ? homeTeam.name : '') : (awayTeam ? awayTeam.name : ''),
                isAwayTeamDisplayedFirst ? minHome : minAway,
                isAwayTeamDisplayedFirst ? maxHome : maxAway,
                isAwayTeamDisplayedFirst ? currentHomeScore : currentAwayScore,
                isAwayTeamDisplayedFirst ? 0 : 1,
              )}
            </Row>
          </Col>
          <Col xs={3} md={3} className="pt-1">
            <div style={{display: 'grid', alignItems: 'end', height: '100%'}}>
              <Btn
                className='m-0'
                text={findScoreInArray()}
                isSelected={tabSelections.some(({isSelected}) => isSelected)}
                responseCode={selectedSelection.responseCode}
                onClick={getSelectedScore}
              />
            </div>
          </Col>
        </Row>
      </Fragment>
    </Fragment>
  )
};
export default CorrectScore;
