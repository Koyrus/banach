import React, {useContext} from 'react';
import {Col, Row} from 'reactstrap';
import _ from 'lodash';
import pBStyles from './popularBets.scss';
import style from '../../templates/PlayerStats/playerStats.scss'
import styleFromFooter from '../../footer/footer.scss'
import Btn from '../../forms/btn/btn';
import MarketsContext from '../../../contexts/MarketsContext';

export default function PopularBets({tabSelections}) {
  const selections = _.orderBy(tabSelections, ['displayOrder'], ['asc']);
  const {setBtnSelected} = useContext(MarketsContext);
  return (
    <div className={pBStyles.popularBets}>
      <div className={`button-gutters ${styleFromFooter.popularList} pb-2 pr-2`}>
        {selections.map((selection, i) => {
          const {
            id, isSelected, priceNum = 0, priceDen = 0, displayName, marketName, responseCode,
          } = selection;
          return (
            <Row key={`${displayName}-${i}`} className={`button-gutters ${styleFromFooter.listItem} pr-0 pl-0`}>
              <Col xs={1} md={1} className={styleFromFooter.colPopular} style={{height: '60px'}}>
                <div className={styleFromFooter.leftSide} style={{paddingTop: '5px'}}>
                  <div className={styleFromFooter.iconareaBlack}>
                    <i className={`${styleFromFooter.iconfa} fa fa-chevron-circle-down`}/>
                  </div>
                </div>
              </Col>
              <Col xs={8} md={8} className={styleFromFooter.paddingCol}>
                <p className={`${style.popularMName} pt-0`}>{displayName}</p>
                <p className={style.popularMarketName}>{marketName}</p>
              </Col>
              <Col xs={3} md={3} className={styleFromFooter.paddingCol} style={{display: 'block', alignItems: 'center'}}>
                <Btn
                  key={`${id}-${i}`}
                  text={`${priceNum} / ${priceDen}`}
                  popular={true}
                  isSelected={isSelected}
                  responseCode={responseCode}
                  onClick={() => setBtnSelected(selection)}
                />
              </Col>
            </Row>
          )
        })}
      </div>
    </div>
  )
}
