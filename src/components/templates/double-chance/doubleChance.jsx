import React, {useContext} from 'react';
import _ from 'lodash';
import {Col, Row} from 'reactstrap';
import MatchBetting from '../match-betting/matchBetting';
import style from '../PlayerStats/playerStats.scss';
import MarketsContext from '../../../contexts/MarketsContext';
import Btn from '../../forms/btn/btn';

export default function DoubleChance(props) {
  const {tabSelections, name, tabs} = props;
  const {setBtnSelected} = useContext(MarketsContext);
  const selections = _.orderBy(tabSelections, ['displayOrder'], ['asc']);
  return <MatchBetting
    {...props}
    customMenuBody={selections.map((selection) => {
      const {
        id, displayName, isSelected, priceNum = 0, priceDen = 0, hundredPcLine, handicapValue, responseCode,
      } = selection;
      return (
        <Row
          key={`${id}-${displayName}`}
          className={`button-gutters mr-1 ml-1 ${tabs.length === 1 ? style.withoutTabs : ''}`}
        >
          <Col xs={9} md={9}>
            <div className={style.totalsLine}>{displayName}</div>
          </Col>
          <Col xs={3} md={3}>
            <Btn
              className={name === 'Total' ? 'mb-2' : ''}
              onClick={() => setBtnSelected(selection)}
              hundredPcLine={hundredPcLine}
              handicapValue={handicapValue}
              responseCode={responseCode}
              text={`${priceNum} / ${priceDen}`}
              isSelected={isSelected}
            />
          </Col>
        </Row>
      )
    })}
  />;
}
