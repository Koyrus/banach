import React, {useContext} from 'react';
import MatchBetting from "../match-betting/matchBetting";
import _ from "lodash";
import {Col, Row} from "reactstrap";
import {Div} from "../../forms/btn/btn";
import MarketsContext from "../../../contexts/MarketsContext";
import Btn from "../../forms/btn/btn";

export default function Handicap(props) {
  const {
    tabSelections, teamNames: { homeTeam, awayTeam }, template, isAwayTeamDisplayedFirst, currentTab,
  } = props;
  const {setBtnSelected} = useContext(MarketsContext);
  const selections = _.orderBy(tabSelections, ['displayOrder'], [isAwayTeamDisplayedFirst ? 'desc' : 'asc']);
  const { drawExactlySelectionDisplayName } = currentTab;

  function getColumnName(relatedTeamId) {
    if (template === 12 || relatedTeamId !== null) {
      if (relatedTeamId === homeTeam.participantId) return homeTeam.name;
      else return awayTeam.name;
    }

    return drawExactlySelectionDisplayName;
  }

  return <MatchBetting {...props} customMenuBody={(
    <Row className="button-gutters mr-1 ml-1">
      {selections.map((selection, i) => {
        const {
          isSelected, hundredPcLine, handicapValue, displayName, priceNum = 0, priceDen = 0, relatedTeamId, responseCode,
        } = selection;
        return (
          <Col xs={template === 12 ? 6 : 4} key={`${displayName}-${i}`}>
            {i < (template === 12 ? 2 : 3) && (
              <Div
                handicap
                hundredPcLine={hundredPcLine}
                handicapValue={handicapValue}
                text={(
                  <div style={{textAlign: 'center'}}>
                    <p className='testName' style={{
                      display: 'inline-block',
                      textOverflow: 'ellipsis',
                      overflow: 'hidden',
                      whiteSpace: 'normal'
                    }}>
                      {getColumnName(relatedTeamId)}
                    </p>
                  </div>
                )}
                isSelected={isSelected}
                responseCode={responseCode}
                className="mb-2"
              />
            )}
            <Btn
              handicap
              hundredPcLine={hundredPcLine}
              handicapValue={handicapValue}
              text={`${priceNum} / ${priceDen}`}
              isSelected={isSelected}
              responseCode={responseCode}
              onClick={() => setBtnSelected(selection)}
              className="mb-2"
            />
          </Col>
        )
      })}
    </Row>
  )} />
};
