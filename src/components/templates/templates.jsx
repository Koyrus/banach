import React from 'react';
import MatchBetting from "./match-betting/matchBetting";
import TotalGoals from "./total-goals/totalGoals";
import CorrectScore from "./correct-score/correctScore";
import DoubleChance from "./double-chance/doubleChance";
import YesNoSelection from "./yesNoSelection/yesNoSelection";
import GoalScorers from "./goal-scorers/goalScorers";
import PlayerStats from "./PlayerStats/playerStats";
import PopularBets from "./popularBets/popularBets";
import Handicap from "./handicap/handicap";
import BookingPoints from "./bookingPoints/bookingPoints";

const Templates = (props) => {
  switch (props.template) {
    case 1:
    case 6:
    case 11:
      return <MatchBetting {...props} name={'Total'}/>;
    case 2:
      return <TotalGoals {...props} name={'Total'}/>;
    case 3:
    case 12:
      return <Handicap {...props} name={'Handicap'}/>;
    case 4:
      return <GoalScorers {...props} name={'GoalScorers'}/>;
    case 5:
      return <PlayerStats {...props} name={'PlayerStats'}/>;
    case 7:
      return <DoubleChance {...props} name={'Double'}/>;
    case 8:
      return <PopularBets {...props} name={'Popular'}/>;
    case 9:
      return <YesNoSelection {...props} name={'Yes'}/>;
    case 10:
      return <CorrectScore {...props} name={'Correct'}/>;
    case 13:
      return <BookingPoints {...props} name={'Booking'}/>;
    default:
      return null;
  }
};
export default Templates;
