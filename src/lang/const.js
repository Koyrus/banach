import LocalizedStrings from 'react-localization';

export const strings = new LocalizedStrings({
    en:{
        ok:"OK",
        add:"Add",
        over:"Over",
        stake:"Stake",
        setStake: "Set Stake",
        betSelected: "Bets Selected",
        betAgain: "Bet Again",
        placeBet: "Place Bet",
        betReceipt: "Bet Receipt",
        anotherBet: "Make another Selection to Build Your Bet",
        reuseSelections: "Reuse Selections",
        potentialReturns: "Potential Returns",
        successBet: 'Bet was successfully placed Good Luck!'
    }
});
