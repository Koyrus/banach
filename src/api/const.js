export const url = 'https://dns-oddsfactory-prod-banachapi.northeurope.cloudapp.azure.com:60320/';
const reactRoot = document.getElementById('banach');
export const clientID = reactRoot.getAttribute('data-clientID');
export const userId = reactRoot.getAttribute('data-userId');
export const lang = navigator.language || navigator.userLanguage;

function createHeader() {
  const header = new Headers();
  header.append('Content-type', 'application/json');
  header.append('api-version', '2.0');
  header.append('clientId', clientID);
  header.append('lang', 'Eng');
  header.append('Cache-Control', 'no-cache');
  return header;
}

export async function getFullAppData(fixtureId) {
  const response = await fetch(url + 'api/AggregateMarkets?fixtureId=' + fixtureId, {headers: createHeader()});
  if (response.ok) return await response.json();
  else throw new Error('Link is broken!');
}

export async function sendSelectionsToCalculate(selectionArray, fixtureId) {
  const rawResponse = await fetch(url + 'api/Price', {
    headers: createHeader(), method: 'POST', body: JSON.stringify(
      {
        "clientFixtureId": fixtureId,
        "selectionIds":
        selectionArray
      })
  });
  if (rawResponse.ok) return await rawResponse.json();
  else throw new Error('Link is broken!');
}

export async function getTabs(fixtureId) {
  const response = await fetch(url + 'api/Grouping?fixtureId=' + fixtureId, {headers: createHeader()});

  if (response.ok) return await response.json();
  else throw new Error('Link is broken!');
}

export async function getMarketsAndTabsNames(id, fixtureId) {
  const response = await fetch(`${url}api/Market?fixtureId=${fixtureId}&groupingId=${id}`, {headers: createHeader()});

  if (response.ok) return await response.json();
  else throw new Error('Link is broken!');
}

export async function getTeamNames(fixtureId) {
  const responseTM = await fetch(`${url}api/Fixture?fixtureId=` + fixtureId, {headers: createHeader()});

  if (responseTM.ok) return await responseTM.json();
  else throw new Error('Link is broken!');
}

export async function getPricesForSelections(visibleSelectionIds, clientFixtureId, betSlipSelectionIds = []) {
  const postData = {
    clientFixtureId,
    betSlipSelectionIds: betSlipSelectionIds.filter(btn => btn !== null && btn !== undefined),
    visibleSelectionIds,
  };

  const rawBetResponse = await fetch(`${url}api/SelectionPrice`, {
    headers: createHeader(), method: 'POST', body: JSON.stringify(postData)
  });

  if (rawBetResponse.ok) return await rawBetResponse.json();
  else throw new Error('Link is broken!');
}

export async function getSelectedPricesValue(clientFixtureId, selectionIds) {
  const response = await fetch(`${url}api/Price`, {
    headers: createHeader(),
    method: 'POST',
    body: JSON.stringify({clientFixtureId, selectionIds}),
  });

  if (response.ok) return await response.json();
  else throw new Error('Link is broken!');
}

export async function getTabRowData(id, fixtureId) {
  const response = await fetch(`${url}api/Selection?fixtureId=` + fixtureId + `&marketId=${id}`, {headers: createHeader()});

  if (response.ok) return await response.json();
  else throw new Error('Link is broken!');
}


export async function getSportTypes() {
  const response = await fetch(`${url}api/Sport`, {headers: createHeader()});
  if (response.ok) return await response.json();
  throw new Error('Link is broken!');
}

export async function getCompetition(sportId) {
  const response = await fetch(`${url}api/Competition?sportId=${sportId}`, {headers: createHeader()});

  if (response.ok) return await response.json();
  else throw new Error('Link is broken!');
}

export async function getCountries(sportId) {
  const response = await fetch(`${url}api/Country?sportId=${sportId}`, {headers: createHeader()});

  if (response.ok) return await response.json();

  throw new Error('Link is broken!');
}

export async function getCompetitionsBySportAndCountry(sportId, country) {
  const response = await fetch(
    `${url}api/Competition/BySportAndCountry?sportId=${sportId}&country=${country}`,
    {headers: createHeader()},
    );

  if (response.ok) return await response.json();

  throw new Error('Link is broken!');
}

export async function fixturesByCompetition(id) {
  const response = await fetch(`${url}api/FixturesByCompetitionId?competitionId=` + id, {headers: createHeader()});

  if (response.ok) return await response.json();
  else throw new Error('Link is broken!');
}

export async function placeBet(clientFixtureId, selectionIds, priceNum = 0, priceDen = 0, stake) {
  const postData = {clientFixtureId, selectionIds, priceNum, userId, priceDen, stake};
  const rawBetResponse = await fetch(`${url}api/placebet`, {
    headers: createHeader(), method: 'POST', body: JSON.stringify(postData)
  });

  if (rawBetResponse.ok) return await rawBetResponse.json();
  else throw new Error('Link is broken!');
}
